import ActionTypes from "shared/consts/ActionTypes";
import addToCollection from "api/addToCollection";
import updateInCollection from "api/updateInCollection";
import removeFromCollection from "api/removeFromCollection";
import fetchCollection from "api/fetchCollection";

export const addTodo = todo => ({
  type: ActionTypes.TODO_ADD,
  payload: { ...todo, isCompleted: false }
});

export const deleteTodo = id => ({
  type: ActionTypes.TODO_DELETE,
  payload: id
});

export const updateTodo = (id, props) => ({
  type: ActionTypes.TODO_UPDATE,
  payload: { id, props }
});

export const updateTodoList = list => ({
  type: ActionTypes.TODO_UPDATE_LIST,
  payload: list
});

export const requestPending = boolean => ({
  type: ActionTypes.REQUEST_PENDING,
  payload: boolean
});

let tempId = 0;
export const requestTodoAdd = todo => dispatch => {
  const temporaryId = `temporaryId-${tempId++}`;

  dispatch(addTodo({ id: temporaryId, ...todo }));
  addToCollection(todo)
    .then(doc => dispatch(updateTodo(temporaryId, { id: doc.id, ...todo })))
    .catch(error => {
      throw Error("Error add todo: ", error);
    });
};

export const requestTodoUpdate = (id, props) => dispatch => {
  dispatch(updateTodo(id, props));
  updateInCollection(id, props).catch(error => {
    throw Error("Error update todo: ", error);
  });
};

export const requestTodoDelete = id => dispatch => {
  removeFromCollection(id)
    .then(() => {
      dispatch(deleteTodo(id));
    })
    .catch(error => {
      throw Error("Error removing todo: ", error);
    });
};

export const requestCollection = () => dispatch => {
  dispatch(requestPending(true));
  fetchCollection(list => {
    dispatch(updateTodoList(list));
    dispatch(requestPending(false));
  });
};
