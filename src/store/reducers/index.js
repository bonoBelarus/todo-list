import ActionTypes from "shared/consts/ActionTypes";

const initialState = {
  todoList: [],
  isRequestPending: false
};

const mapActionToReducer = (state, action) => ({
  [ActionTypes.TODO_ADD]: {
    ...state,
    todoList: [...state.todoList, action.payload]
  },
  [ActionTypes.TODO_UPDATE_LIST]: {
    ...state,
    todoList: action.payload
  },
  [ActionTypes.TODO_UPDATE]: {
    ...state,
    todoList: [...state.todoList].map(todo => {
      if (todo.id === action.payload.id) {
        return { ...todo, ...action.payload.props };
      }
      return todo;
    })
  },
  [ActionTypes.TODO_DELETE]: {
    ...state,
    todoList: state.todoList.filter(todo => todo.id !== action.payload)
  },
  [ActionTypes.REQUEST_PENDING]: {
    ...state,
    isRequestPending: action.payload
  }
});

export default (state = initialState, action) =>
  mapActionToReducer(state, action)[action.type] || state;
