import React from "react";
import Button from "react-bootstrap/Button";
import { default as BootstrapForm } from "react-bootstrap/Form";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { func } from "prop-types";

import { requestTodoAdd } from "store/actions";
import changeTextAreaHeight from "shared/utils/changeTextAreaHeight";

class Form extends React.Component {
  static propTypes = {
    requestTodoAdd: func.isRequired
  };

  state = {
    title: "",
    description: "",
    validated: false
  };

  handleChange = target => {
    this.setState({ [target.name]: target.value });

    if (target.tagName.toLowerCase() === "textarea") {
      changeTextAreaHeight(target);
    }
  };

  resetForm = () =>
    this.setState({
      title: "",
      description: "",
      validated: false
    });

  onSubmit = event => {
    event.preventDefault();
    const { requestTodoAdd } = this.props;

    if (event.currentTarget.checkValidity()) {
      const title = event.target.title.value;
      const description = event.target.description.value;

      requestTodoAdd({ title, description, isCompleted: false });
      this.resetForm();
    } else {
      this.setState({
        validated: true
      });
    }
  };

  render() {
    const { title, description, validated } = this.state;

    return (
      <BootstrapForm
        noValidate
        validated={validated}
        className="d-flex flex-column justify-content-end flex-wrap"
        onSubmit={event => this.onSubmit(event)}
      >
        <BootstrapForm.Group>
          <BootstrapForm.Control
            required
            as="textarea"
            name="title"
            value={title}
            placeholder="Title"
            onChange={({ target }) => this.handleChange(target)}
          />
          <BootstrapForm.Control.Feedback type="invalid">
            Please fill in the field.
          </BootstrapForm.Control.Feedback>
        </BootstrapForm.Group>
        <BootstrapForm.Group className="mt-2">
          <BootstrapForm.Control
            required
            as="textarea"
            value={description}
            placeholder="Description"
            textarea="true"
            name="description"
            onChange={({ target }) => this.handleChange(target)}
          />
          <BootstrapForm.Control.Feedback type="invalid">
            Please fill in the field.
          </BootstrapForm.Control.Feedback>
        </BootstrapForm.Group>
        <Button
          aria-label="submit form"
          variant="primary"
          type="submit"
          className="align-self-end mt-2"
        >
          Add
        </Button>
      </BootstrapForm>
    );
  }
}

export default connect(
  null,
  dispatch =>
    bindActionCreators(
      {
        requestTodoAdd
      },
      dispatch
    )
)(Form);
