import React from "react";
import ListGroup from "react-bootstrap/ListGroup";
import Spinner from "react-bootstrap/Spinner";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { shape, arrayOf, string, func, bool } from "prop-types";
import { SortableElement, SortableContainer } from "react-sortable-hoc";
import arrayMove from "array-move";

import Todo from "components/Todo";
import conditionalFilter from "shared/utils/conditionalFilter";

import {
  requestCollection,
  updateTodo,
  addTodo,
  deleteTodo,
  updateTodoList
} from "store/actions";
import listenCollection from "api/listenCollection";
import Firebase from "shared/consts/Firebase";

const SortableItem = SortableElement(({ todo }) => <Todo {...todo} />);

const SortableList = SortableContainer(({ items }) => {
  return (
    <ListGroup variant="flush" className="mt-2">
      {items.map((todo, index) => (
        <SortableItem key={todo.id} index={index} todo={todo} />
      ))}
    </ListGroup>
  );
});

class TodoList extends React.Component {
  static propTypes = {
    items: arrayOf(shape({})),
    tab: string.isRequired,
    todoList: arrayOf(shape({})).isRequired,
    requestCollection: func.isRequired,
    updateTodoList: func.isRequired,
    updateTodo: func.isRequired,
    addTodo: func.isRequired,
    deleteTodo: func.isRequired,
    isRequestPending: bool.isRequired
  };

  static defaultProps = {
    items: []
  };

  componentDidMount() {
    const { requestCollection, updateTodo, addTodo, deleteTodo } = this.props;

    requestCollection();
    listenCollection(Firebase.listen.MODIFIED, (id, props) =>
      updateTodo(id, props)
    );
    listenCollection(Firebase.listen.ADDED, (id, props) =>
      addTodo({ id, ...props })
    );
    listenCollection(Firebase.listen.REMOVED, id => deleteTodo(id));
  }

  onSortEnd = ({ oldIndex, newIndex }) => {
    const { updateTodoList, items, todoList, tab } = this.props;

    updateTodoList([
      ...todoList.filter(todo => !conditionalFilter(todo)[tab]),
      ...arrayMove(items, oldIndex, newIndex)
    ]);
  };

  render() {
    const { items, isRequestPending } = this.props;

    if (isRequestPending) {
      return (
        <div className="d-flex justify-content-center">
          <Spinner variant="primary" animation="border" role="status" />
        </div>
      );
    }

    return items.length ? (
      <SortableList
        items={items}
        onSortEnd={this.onSortEnd}
        lockAxis="y"
        useDragHandle
      />
    ) : (
      <div className="d-flex justify-content-center">No Items</div>
    );
  }
}

export default connect(
  ({ todoList, isRequestPending }) => ({
    todoList,
    isRequestPending
  }),
  dispatch =>
    bindActionCreators(
      {
        requestCollection,
        updateTodoList,
        updateTodo,
        addTodo,
        deleteTodo
      },
      dispatch
    )
)(TodoList);
