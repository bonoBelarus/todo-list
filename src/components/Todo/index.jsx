/* eslint-disable react/destructuring-assignment */
import React from "react";
import ListGroup from "react-bootstrap/ListGroup";
import Button from "react-bootstrap/Button";
import FormControl from "react-bootstrap/FormControl";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { string, func, bool } from "prop-types";
import { sortableHandle } from "react-sortable-hoc";

import { requestTodoUpdate, requestTodoDelete } from "store/actions";
import changeTextAreaHeight from "shared/utils/changeTextAreaHeight";

import "./index.scss";

const DragHandle = sortableHandle(() => (
  <span className="mr-3 drag-button">
    <i className="fas fa-arrows-alt" />
  </span>
));

class Todo extends React.PureComponent {
  static propTypes = {
    id: string.isRequired,
    title: string.isRequired,
    isCompleted: bool.isRequired,
    description: string.isRequired,
    requestTodoDelete: func.isRequired,
    requestTodoUpdate: func.isRequired
  };

  state = {
    editable: false,
    title: this.props.title,
    description: this.props.description,
    isCompleted: this.props.isCompleted
  };

  componentDidMount() {
    changeTextAreaHeight([this.titleElement, this.descriptionElement]);
  }

  componentWillReceiveProps(nextProps) {
    const { title, description, isCompleted } = this.props;

    title !== nextProps.title &&
      this.setState(
        {
          title: nextProps.title
        },
        () => changeTextAreaHeight(this.titleElement)
      );

    description !== nextProps.description &&
      this.setState(
        {
          description: nextProps.description
        },
        () => changeTextAreaHeight(this.descriptionElement)
      );

    isCompleted !== nextProps.isCompleted &&
      this.setState({
        isCompleted: nextProps.isCompleted
      });
  }

  handleEdit = () =>
    this.setState({
      editable: true
    });

  handleSave = () => {
    const { id, requestTodoUpdate } = this.props;
    const { title, description } = this.state;

    this.setState({
      editable: false
    });

    requestTodoUpdate(id, {
      title,
      description
    });
  };

  toggleComplete = () => {
    const { id, requestTodoUpdate } = this.props;
    const { isCompleted } = this.state;

    this.setState(({ isCompleted }) => ({
      isCompleted: !isCompleted
    }));

    requestTodoUpdate(id, {
      isCompleted: !isCompleted
    });
  };

  handleChange = ({ target }) => {
    this.setState({
      [target.name]: target.value
    });

    target.tagName.toLowerCase() === "textarea" && changeTextAreaHeight(target);
  };

  render() {
    const { id, requestTodoDelete } = this.props;
    const { title, description, editable, isCompleted } = this.state;

    return (
      <ListGroup.Item className={`todo-item ${editable ? "editable" : ""}`}>
        <div className="d-flex">
          <DragHandle />
          <Button
            aria-label="complete"
            size="sm"
            variant={isCompleted ? "warning" : "success"}
            className="mr-3"
            onClick={() => this.toggleComplete()}
          >
            {isCompleted ? (
              <i className="fas fa-calendar-times" />
            ) : (
              <i className="fas fa-calendar-check" />
            )}
          </Button>
          {editable ? (
            <Button
              aria-label="save"
              size="sm"
              variant="success"
              className="mr-3"
              onClick={() => this.handleSave()}
            >
              <i className="fas fa-check" />
            </Button>
          ) : (
            <Button
              aria-label="edit"
              size="sm"
              className="mr-3"
              variant="light"
              onClick={() => this.handleEdit()}
            >
              <i className="fas fa-edit" />
            </Button>
          )}
          <Button
            aria-label="remove"
            variant="danger"
            size="sm"
            onClick={() => requestTodoDelete(id)}
          >
            <i className="fas fa-trash-alt" />
          </Button>
        </div>
        <div className="mt-2">
          <FormControl
            ref={ref => (this.titleElement = ref)}
            as="textarea"
            label="rch"
            value={title}
            title={title}
            name="title"
            disabled={!editable}
            className={`text-body font-weight-bold ${editable ? "active" : ""}`}
            onChange={e => this.handleChange(e)}
          />
        </div>
        <div className="mt-2">
          <FormControl
            ref={ref => (this.descriptionElement = ref)}
            as="textarea"
            name="description"
            value={description}
            textarea="true"
            disabled={!editable}
            className={`text-body ${editable ? "active" : ""}`}
            onChange={e => this.handleChange(e)}
          />
        </div>
      </ListGroup.Item>
    );
  }
}

export default connect(
  null,
  dispatch =>
    bindActionCreators(
      {
        requestTodoUpdate,
        requestTodoDelete
      },
      dispatch
    )
)(Todo);
