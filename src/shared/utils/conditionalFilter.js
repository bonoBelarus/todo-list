export default todo => ({
  All: todo,
  "To do": todo.isCompleted !== true,
  Completed: todo.isCompleted === true
});
