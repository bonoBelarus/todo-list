/* eslint-disable no-param-reassign */
const modifiedHeight = target => {
  target.style.height = "1px";
  target.style.height = `${5 + target.scrollHeight}px`;
};

export default function changeTextAreaHeight(obj) {
  if (Array.isArray(obj)) {
    obj.forEach(target => modifiedHeight(target));
  } else {
    modifiedHeight(obj);
  }
}
