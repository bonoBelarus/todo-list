export default class Firebase {
  static get config() {
    return {
      API_KEY: "AIzaSyArVW-cYvQa7zSXv_bsAUJc0xMw6zVPhd4",
      AUTH_DOMAIN: "todo-list-9505b.firebaseapp.com",
      DATABASE_URL: "https://todo-list-9505b.firebaseio.com",
      PROJECT_ID: "todo-list-9505b",
      STORAGE_BUCKET: "todo-list-9505b.appspot.com",
      MESSAGING_SENDER_ID: "936083877850"
    };
  }

  static get listen() {
    return {
      ADDED: "added",
      REMOVED: "removed",
      MODIFIED: "modified"
    };
  }

  static get TODOS_COLLECTION_NAME() {
    return "todos";
  }
}
