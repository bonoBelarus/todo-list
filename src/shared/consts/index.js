export default class Consts {
  static get TABS() {
    return ["All", "To do", "Completed"];
  }
}
