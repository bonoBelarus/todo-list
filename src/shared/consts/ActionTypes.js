export default class ActionTypes {
  static get TODO_ADD() {
    return "TODO_ADD";
  }

  static get TODO_DELETE() {
    return "TODO_DELETE";
  }

  static get TODO_UPDATE() {
    return "TODO_UPDATE";
  }

  static get TODO_UPDATE_LIST() {
    return "TODO_UPDATE_LIST";
  }

  static get REQUEST_PENDING() {
    return "REQUEST_PENDING";
  }
}
