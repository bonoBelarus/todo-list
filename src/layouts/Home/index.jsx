import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import withTabs from "hoc/withTabs";
import Form from "components/Form";
import TodoList from "components/TodoList";
import Consts from "shared/consts";

const TodoListWithTabs = withTabs(TodoList, Consts.TABS);

export default function Home() {
  return (
    <Container className="p-4">
      <Row className="justify-content-center">
        <Col xs={12} sm={8} md={6} lg={5} xl={5} className="d-flex flex-column">
          <div className="align-self-end w-25">
            <img
              src="images/todo.png"
              className="mw-100 d-block"
              alt=""
              title="logo"
            />
          </div>
          <h2>Todo list</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col xs={12} sm={8} md={6} lg={5} xl={5}>
          <Form />
        </Col>
      </Row>
      <TodoListWithTabs />
    </Container>
  );
}
