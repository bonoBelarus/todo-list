import { db } from "firebaseInitializer";
import Firebase from "shared/consts/Firebase";

export default (type, callback) =>
  db.collection(Firebase.TODOS_COLLECTION_NAME).onSnapshot(snapshot =>
    snapshot.docChanges().forEach(change => {
      const source = snapshot.metadata.hasPendingWrites ? "local" : "server";

      change.type === type &&
        source === "server" &&
        callback(change.doc.id, change.doc.data());
    })
  );
