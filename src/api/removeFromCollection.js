import { db } from "firebaseInitializer";
import Firebase from "shared/consts/Firebase";

export default id =>
  db
    .collection(Firebase.TODOS_COLLECTION_NAME)
    .doc(id)
    .delete();
