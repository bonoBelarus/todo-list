import { db } from "firebaseInitializer";
import Firebase from "shared/consts/Firebase";

export default callback =>
  db
    .collection(Firebase.TODOS_COLLECTION_NAME)
    .get()
    .then(querySnapshot => {
      const list = [];

      querySnapshot.forEach(doc => {
        list.push({ id: doc.id, ...doc.data() });
      });

      callback(list);
    });
