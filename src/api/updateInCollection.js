import { db } from "firebaseInitializer";
import Firebase from "shared/consts/Firebase";

export default (id, props) =>
  db
    .collection(Firebase.TODOS_COLLECTION_NAME)
    .doc(id)
    .update(props);
