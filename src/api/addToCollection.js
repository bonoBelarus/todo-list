import { db } from "firebaseInitializer";
import Firebase from "shared/consts/Firebase";

export default todo => db.collection(Firebase.TODOS_COLLECTION_NAME).add(todo);
