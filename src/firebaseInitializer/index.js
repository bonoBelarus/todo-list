import firebase from "firebase/app";
import "firebase/firestore";
import Firebase from "shared/consts/Firebase";

export default firebase.initializeApp({
  apiKey: Firebase.config.API_KEY,
  authDomain: Firebase.config.AUTH_DOMAIN,
  databaseURL: Firebase.config.DATABASE_URL,
  projectId: Firebase.config.PROJECT_ID,
  storageBucket: Firebase.config.STORAGE_BUCKET,
  messagingSenderId: Firebase.config.MESSAGING_SENDER_ID
});

firebase.firestore().enablePersistence();

export const db = firebase.firestore();
