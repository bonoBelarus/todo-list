import React, { Fragment } from "react";
import { connect } from "react-redux";
import { shape, arrayOf } from "prop-types";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Nav from "react-bootstrap/Nav";

import conditionalFilter from "shared/utils/conditionalFilter";

export default function withTabs(WrappedComponent, tabs) {
  class EnhancedComponent extends React.Component {
    static propTypes = {
      todoList: arrayOf(shape({})).isRequired
    };

    static getDerivedStateFromProps(props, state) {
      if (props.todoList !== state.items) {
        return {
          items: props.todoList,
          filteredItems: props.todoList.filter(
            todo => conditionalFilter(todo)[state.active]
          )
        };
      }

      return null;
    }

    state = {
      active: tabs[0],
      items: []
    };

    onSelectTab = selectedKey => {
      const { todoList } = this.props;

      this.setState({
        active: selectedKey,
        filteredItems: todoList.filter(
          todo => conditionalFilter(todo)[selectedKey]
        )
      });
    };

    render() {
      const { filteredItems, active } = this.state;

      return (
        <Fragment>
          <Row className="justify-content-center py-4">
            <Col xs={12} sm={8} md={6} lg={5} xl={5}>
              <Nav
                variant="tabs"
                defaultActiveKey={active}
                onSelect={selectedKey => this.onSelectTab(selectedKey)}
              >
                {tabs.map(tab => (
                  <Nav.Item key={tab}>
                    <Nav.Link eventKey={tab} onClick={e => e.preventDefault()}>
                      {tab}
                    </Nav.Link>
                  </Nav.Item>
                ))}
              </Nav>
            </Col>
          </Row>
          <Row className="justify-content-center">
            <Col xs={12} sm={8} md={6} lg={5} xl={5}>
              <WrappedComponent
                {...this.props}
                items={filteredItems}
                tab={active}
              />
            </Col>
          </Row>
        </Fragment>
      );
    }
  }

  return connect(({ todoList }) => ({
    todoList
  }))(EnhancedComponent);
}
