const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack");
const { GenerateSW } = require("workbox-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  mode: "development",
  entry: ["./src/index.jsx"],
  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "dist")
  },
  resolve: {
    extensions: [".js", ".jsx"],
    modules: [path.resolve(__dirname, "node_modules"), "src"]
  },
  devServer: {
    contentBase: path.join(__dirname, "public"),
    hot: true
  },
  module: {
    rules: [
      {
        test: /\.(scss|sass)$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader" },
          {
            loader: "sass-loader",
            options: {
              includePaths: ["src/styles"]
            }
          }
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: "file-loader"
      },
      {
        test: /\.(js|jsx)$/,
        use: "babel-loader"
      },
      {
        test: /\.(js|jsx)$/,
        use: "eslint-loader"
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "public/index.html"
    }),
    new webpack.HotModuleReplacementPlugin(),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, "public/images"),
        to: path.join(__dirname, "dist/images")
      },
      {
        from: path.join(__dirname, "public/favicons"),
        to: path.join(__dirname, "dist/favicons")
      },
      {
        from: path.join(__dirname, "public/manifest.json"),
        to: path.join(__dirname, "dist/manifest.json")
      }
    ]),
    new GenerateSW({
      swDest: "service-worker.js"
    })
  ]
};
