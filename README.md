# TODO-List

* Layout: `react-bootstrap`.
* Backend: `firebase`.
* Database: `firestore`.
* Hosting: [https://todo-list-9505b.firebaseapp.com/](https://todo-list-9505b.firebaseapp.com/).

Синхронизация между устройствами реализована с помощью [API firestore](https://firebase.google.com/docs/firestore).

Кэширование реализовано с помощью [workbox](https://developers.google.com/web/tools/workbox/) и `workbox-webpack-plugin`.


## Scripts:

### `npm install`

Установка всех зависимостей

### `npm start`

Запуск проекта по адресу [http://localhost:8080](http://localhost:8080).
